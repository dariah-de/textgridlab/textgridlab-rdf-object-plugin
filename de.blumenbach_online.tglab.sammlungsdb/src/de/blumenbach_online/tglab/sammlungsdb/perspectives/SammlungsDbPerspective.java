/*
 * #%L
 * info.textgrid.lab.sadepublish
 * %%
 * Copyright (C) 2011 TextGrid Consortium (http://www.textgrid.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package de.blumenbach_online.tglab.sammlungsdb.perspectives;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class SammlungsDbPerspective implements IPerspectiveFactory{

	@Override
	public void createInitialLayout(IPageLayout layout) {
		// TODO Auto-generated method stub
		
    	String editorArea = layout.getEditorArea();
    	
    	IFolderLayout topLeft = 
    		layout.createFolder("topLeft", IPageLayout.LEFT, (float)0.3, editorArea);//$NON-NLS-1$
       	topLeft.addView("info.textgrid.lab.navigator.view");
 
    	/*IFolderLayout topRight= 
    		layout.createFolder("topRight", IPageLayout.RIGHT, (float)0.4, editorArea);//$NON-NLS-1$
       	topRight.addView("de.blumenbach_online.tglab.sammlungsdb.views.SammlungsDbEditor");*/
    	
    	layout.setEditorAreaVisible(true); 
	}

}
