/*
 * #%L
 * info.textgrid.lab.sadepublish
 * %%
 * Copyright (C) 2011 TextGrid Consortium (http://www.textgrid.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package de.blumenbach_online.tglab.sammlungsdb.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.blumenbach_online.tglab.sammlungsdb.Activator;

public class PluginPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage{

	public static String boldbUrl_id = "boldb_url";
	//public static String boldbUser_id = "boldb_user";
	//public static String boldbPW_id = "boldb_pw";
	
	@Override
	protected void createFieldEditors() {

		addField(new StringFieldEditor(boldbUrl_id, "URL to RDF-Object Inputform", getFieldEditorParent()));
		//addField(new StringFieldEditor(boldbUser_id, "Authorized user", getFieldEditorParent()));
		//addField(new StringFieldEditor(boldbPW_id, "Password", getFieldEditorParent()));	
		
	}

	@Override
	public void init(IWorkbench workbench) {

		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		
	}

}
