# RDF Object Editor Plugin for TextGridLab

This is a TextGridLab plugin to connect to a RDF editing form from TextGridLab.

To use this plugin activate it from the marketplace inside TextGridLab or add https://dariah-de.pages.gwdg.de/textgridlab/textgridlab-rdf-object-plugin/ as an update site to TextGridLab.

## Release / Development

This project uses the [Git-Flow Maven plugin](https://github.com/aleksandr-m/gitflow-maven-plugin)

For a list of goals enter

        mvn gitflow:help

start a new feature

        mvn gitflow:feature-start

finish feature

        mvn gitflow:feature-finish

finish and squash commits

        mvn gitflow:feature-finish -DfeatureSquash=true

### Versioning

Versions should be managed by maven-gitflow-plugin. If you need to change a
version manually you may do with
[Tycho Versions Plugin](https://www.eclipse.org/tycho/sitedocs/tycho-release/tycho-versions-plugin/set-version-mojo.html):

        mvn tycho-versions:set-version -DnewVersion=0.0.3-SNAPSHOT
