package de.blumenbach_online.tglab.sammlungsdb.views;

import java.util.ArrayList;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.browser.StatusTextEvent;
import org.eclipse.swt.browser.StatusTextListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.progress.UIJob;

import de.blumenbach_online.tglab.sammlungsdb.Activator;
import de.blumenbach_online.tglab.sammlungsdb.preferences.PluginPreferencePage;

public class SammlungsDbEditor extends EditorPart {
	
	private Browser browser;
	private boolean browserReady = false;
	private ArrayList<UIJob> browserJobs = new ArrayList<UIJob>();
	private boolean dirty = false;

	@Override
	public void doSave(IProgressMonitor arg0) {
		scheduleJavascript("save();");
		setDirty(false);
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		try {
			PlatformUI.getWorkbench().showPerspective(
					"de.blumenbach_online.tglab.sammlungsdb.perspectives.SammlungsDbPerspective",
					PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow());
		} catch (WorkbenchException e) {
			Activator.handleError(e, "error opening sammlungsdbperspective");
		}
		
		TextGridObject tgo = (TextGridObject) input
				.getAdapter(TextGridObject.class);
		if (tgo != null) {
			
			SammlungsDbEditorInput sdbeInput = new SammlungsDbEditorInput(tgo);
			setSite(site);
			setInput(sdbeInput);
			
			try {
				this.setPartName(tgo.getTitle());				
				scheduleJavascript("open('"+tgo.getURI().toString()+"');");
			} catch (CoreException e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					    "Error !", e);
				Activator.getDefault().getLog().log(status);
			}

		}
		
	}

	@Override
	public boolean isDirty() {
		// TODO Auto-generated method stub
		//return false;
		//scheduleJavascript("isDirty();");
		/*Boolean dirty = false;
		if(browserReady){
			try {
				Object bdirty = browser.evaluate("isDirty()");
				System.out.println("bready " + bdirty + " - " + bdirty.getClass().getName());
			} catch(Exception e) {
				System.out.println("catched " + e.getMessage());
			}
			
			//System.out.println(bready);
		}*/
			
		//System.out.println("isDirty called " + dirty);
		
		return dirty;
	}
	
	private void setDirty(boolean dirty) {
		if (dirty != this.dirty) {
			this.dirty = dirty;
			firePropertyChange(IEditorPart.PROP_DIRTY);
		}
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {
	   	try {
    		browser = new Browser(parent, SWT.NONE);
    	} catch (SWTError e) {
    		Activator.handleError(e, "Could not instantiate Browserview");
			return;
		}

    	String url = Platform.getPreferencesService().getString(Activator.PLUGIN_ID, PluginPreferencePage.boldbUrl_id, "", null);
    	
    	if(url.equals("")) {
    		browser.setText("No RDF-Object-Editor Server set. Please set with Window -> Preferences -> RDF-Object Inputform -> URL to RDF-Object Inputform");
    		return;
    	}
    	
    	browser.setUrl(url);
		browser.addProgressListener(new ProgressListener() {
			public void changed(ProgressEvent arg0) {}			
			public void completed(ProgressEvent arg0) {

				String sid = RBACSession.getInstance().getSID(false);
				//System.out.println("set sid to: " + sid);
				browser.execute("setSid('"+sid+"')");

				//String user = Platform.getPreferencesService().getString(Activator.PLUGIN_ID, PluginPreferencePage.boldbUser_id, "", null);
				//String pw = Platform.getPreferencesService().getString(Activator.PLUGIN_ID, PluginPreferencePage.boldbPW_id, "", null);
                //browser.execute("setAuth('"+user+"','"+pw+"')");

				browserReady = true;
				
				for(UIJob job : browserJobs) {
					job.schedule();
				}
			}  			
		});
		
		/*browser.addStatusTextListener(new StatusTextListener() {

			@Override
			public void changed(StatusTextEvent event) {
				// TODO Auto-generated method stub
				System.out.println("stl: " +event + event.text);
			}
			
		});*/
		
		
		browser.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				setDirty(true);
			}

			@Override
			public void keyReleased(KeyEvent e) {
				//dirty = true;
				//System.out.println("key released");
				
			}
			
			
		});
    	
	}
	
	/**
	 * execute javascript only is browser is ready, otherwise put it to joblist
	 * @param js
	 */
	public void scheduleJavascript(final String js) {

		UIJob execjs = new UIJob("executing Javasscript"){

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				browser.execute(js);
				return Status.OK_STATUS;
			}
			
		};
		
		registerBrowserJob(execjs);
	}
	
	public void registerBrowserJob(UIJob job) {
		if(browserReady) job.schedule();
		else browserJobs.add(job);
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

}
